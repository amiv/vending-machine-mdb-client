import http.client
import logging
from connectors import IdProvider

class VmpID(IdProvider):
    orgname = "vmp"


    def __init__(self):
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.log = logging.getLogger(__name__)
        self.log.setLevel(self.loglevel)

        config = self.get_config('vmp')
        self.key = config.get('vmp', 'key')


    def __getRequest(self, url):
        # connect to vmp.ethz.ch and send request
        # try to do so over ssl
        try:
            import ssl 
            con = http.client.HTTPSConnection("coffee.vmp.ethz.ch")
        # if ssl is not available use normal http
        except ImportError:
            con = http.client.HTTPConnection("coffee.vmp.ethz.ch")
    
        con.request("GET", url + "&token={}".format(self.key))
        res = con.getresponse()
        # return response as string (not bytes) and remove '\n' etc.
        return str(res.read(), "utf-8").strip()


    # @brief allow or deny beer for given rfid
    # @param rfid: check for that RFID
    # @return True if beer is allowed, False otherwise
    def auth(self, rfid: int) -> int:
        try:
            # logging
            self.log.debug("Trying to authenticate RFID {}...".format(rfid))
    
            # request: returns beer == 1 if beer is allowed, 0 otherwise
            url = "/beer/check/?rfid={}".format(rfid)
            beer = self.__getRequest(url)

            return int(beer)
        except Exception:
            return None


    # @brief report that a user got his beer
    # @param rfid: RFID of user who got something from the beer dispenser
    # @return True if it worked, False otherwise
    def report(self, rfid: int) -> bool:
        # request: return res == 1 if everything worked
        url = "/beer/record/?rfid={}&machine=cab".format(rfid)
        res = self.__getRequest(url)

        return int(res) == 1
