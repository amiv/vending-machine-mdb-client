from typing import Sequence, Any, Callable, Union, Tuple
from threading import Lock
from enum import Enum
import struct
import logging

from .MDBCommunicator import MDBCommunicator, EnqueuedMessage
from .MDBCommands import MDBCommand, MDBSubcommand, MDBMessageCreator


class MDBState(Enum):
    RESET        = 1
    DISABLED     = 2
    ENABLED      = 3
    SESSION_IDLE = 4
    VEND         = 5


class MDBVendRequest():
    _counter = 0

    def __init__(self, slot: int):
        self.id = self._counter
        self.slot = slot
        self._counter += 1


class MDBHandler():

    def __init__(self, pi: Any, rx_gpio: int, tx_gpio: int,
                 handle_state_change: Callable[[MDBState, MDBState], None],
                 handle_vend_request: Callable[[MDBVendRequest], Union[bool, None]],
                 handle_vend_completed: Callable[[bool], None], address: int = 0x60):
        """
        Callback functions (should contain just as much code as really needed!):
         * handle_state_change(newState, previousState)
         * handle_vend_request(vendRequest)
             NOTE: return True/False to responde directly or return None to respond later with a callback function
         * handle_vend_completed(vendSuccessful)
             NOTE: first parameter specifies whether the product has been dispensed or if it has failed.
        """
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        self.communicator = MDBCommunicator(pi, rx_gpio, tx_gpio, self._handle_frame, address)
        self.state = MDBState.RESET
        self.state_lock = Lock()
        self.vend_request = None
        self.handle_state_change = handle_state_change
        self.handle_vend_request = handle_vend_request
        self.handle_vend_completed = handle_vend_completed


    def start(self) -> None:
        with self.state_lock:
            self.state = MDBState.RESET
        self.communicator.start()
        self.logger.info('MDBHandler started.')


    def stop(self) -> None:
        if self.communicator.is_running:
            self.communicator.stop()
            self.logger.info('MDBHandler stopping...')


    def isAlive(self) -> bool:
        return self.communicator.isAlive()


    def join(self, timeout: int = None) -> None:
        self.communicator.join(timeout)


    def __del__(self):
        self.stop()


    def reset(self) -> None:
        with self.state_lock:
            self.state = MDBState.RESET
        self.communicator.reset()
        self.logger.debug('MDBHandler resetted.')


    def open_session(self, display_content: Union[Sequence[int], None] = None, time: int = 6000) -> None:
        # time in milliseconds!
        if (self.state == MDBState.ENABLED):

            def callback(successful: bool) -> bool:
                if successful:
                    self._set_state(MDBState.SESSION_IDLE)
                return not successful

            self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.sessionStart(), callback))
            if display_content is not None:
                self.update_display(display_content, time)
            self.logger.debug('MDB session opened!')


    def update_display(self, content: Sequence[int], time: int = 6000) -> None:
        # time in milliseconds!
        if self.state == MDBState.SESSION_IDLE or self.state == MDBState.VEND or self.state == MDBState.ENABLED:
            self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.sessionDisplayRequest(time, content)))
            self.logger.debug('MDB display updated!')


    def cancel_session(self) -> None:
        if (self.state == MDBState.SESSION_IDLE or self.state == MDBState.VEND):
            self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.sessionCancel()))
            self.logger.debug("MDB session cancellation requested!")


    def approve_vend_request(self, vend_request_id: int) -> None:
        if (self.vend_request != None and vend_request_id == self.vend_request.id and self.state == MDBState.VEND):
            self.vend_request = None
            self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.vendApprove()))


    def deny_vend_request(self, vend_request_id: int) -> None:
        if (self.vend_request != None and vend_request_id == self.vend_request.id and self.state == MDBState.VEND):
            self.vend_request = None
            self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.vendDeny()))


    def get_state(self) -> MDBState:
        return self.state


    def get_vend_request(self) -> MDBVendRequest:
        return self.vend_request


    def _set_state(self, state: MDBState) -> None:
        previousState = None
        with self.state_lock:
            previousState = self.state
            self.state = state

            if state == MDBState.SESSION_IDLE:
                # Reset vend request when opening a new session
                # or a vend request has been completed.
                self.vend_request = None

        self.logger.debug('MDB state changed from ' + str(previousState) + ' to ' + str(state))
        self.handle_state_change(state, previousState)


    def _handle_frame(self, command: int, length: int, frame: Sequence[int]) -> None:
        if command == MDBCommand.POLL:
            if self.state == MDBState.RESET:
                if self.communicator.send_message(MDBMessageCreator.justReset()):
                    self._set_state(MDBState.DISABLED)
            elif self.state != MDBState.DISABLED and self.communicator.has_enqueued_messages():
                self.communicator.send_enqueued_messages()
            else:
                self.communicator.send_ack()
        elif command == MDBCommand.VEND:
            if frame[1] == MDBSubcommand.VEND_REQUEST:
                if self.state == MDBState.SESSION_IDLE:
                    slot = frame[4] << 8 | frame[5]
                    self.vend_request = MDBVendRequest(slot)
                    self._set_state(MDBState.VEND)

                    response = self.handle_vend_request(self.vend_request)
                    if response is None:
                        self.communicator.send_ack()
                    else:
                        if response is True:
                            self.communicator.send_message(MDBMessageCreator.vendApprove())
                        else:
                            self.communicator.send_message(MDBMessageCreator.vendDeny())
                        self.vend_request = None
                else:
                    self.communicator.send_ack()
            elif frame[1] == MDBSubcommand.VEND_CANCEL:
                if self.state == MDBState.VEND:

                    def callback(successful: bool) -> bool:
                        if successful:
                            self._set_state(MDBState.SESSION_IDLE)
                        return not successful

                    self.vend_request = None
                    self.communicator.send_ack()
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.vendDeny(), callback))
                else:
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.outOfSequence()))
            elif frame[1] == MDBSubcommand.VEND_SUCCESS:
                if self.state == MDBState.VEND:
                    self.communicator.send_ack()
                    self._set_state(MDBState.SESSION_IDLE)
                    self.handle_vend_completed(True)
                else:
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.outOfSequence()))
            elif frame[1] == MDBSubcommand.VEND_FAILURE:
                if self.state == MDBState.VEND:
                    self.communicator.send_ack()
                    self._set_state(MDBState.SESSION_IDLE)
                    self.handle_vend_completed(False)
                else:
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.outOfSequence()))
            elif frame[1] == MDBSubcommand.VEND_SESSION_COMPLETE:
                if self.state == MDBState.SESSION_IDLE:

                    def callback(successful: bool) -> bool:
                        if successful:
                            self._set_state(MDBState.ENABLED)
                        return not successful

                    self.communicator.send_ack()
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.sessionEnd(), callback))
                else:
                    self.communicator.enqueue_message(EnqueuedMessage(MDBMessageCreator.outOfSequence()))
        elif command == MDBCommand.READER:
            if frame[1] == MDBSubcommand.READER_ENABLE:
                if self.state == MDBState.DISABLED:
                    self.communicator.send_ack()
                    self._set_state(MDBState.ENABLED)
                else:
                    self.communicator.send_nack()
            elif frame[1] == MDBSubcommand.READER_DISABLE:
                if self.state != MDBState.DISABLED:
                    self.communicator.send_ack()
                    self._set_state(MDBState.DISABLED)
            elif frame[1] == MDBSubcommand.READER_CANCEL:
                if self.state != MDBState.DISABLED:
                    self.communicator.send_message([0x08])
                    self._set_state(MDBState.ENABLED)
                else:
                    self.communicator.send_nack()
        elif command == MDBCommand.SETUP:
            if frame[1] == MDBSubcommand.SETUP_CONFIG_DATA:
                self.communicator.send_message(MDBMessageCreator.setupConfigData())
            elif frame[1] == MDBSubcommand.SETUP_MAX_MIN_PRICES:
                self.communicator.send_ack()
        elif command == MDBCommand.EXPANSION:
            if frame[1] == MDBSubcommand.EXPANSION_REQUEST_ID:
                self.communicator.send_message(MDBMessageCreator.expansionRequestId())
        elif command == MDBCommand.RESET:
            self.communicator.send_ack()
            self.reset()
