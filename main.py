import os
import sys
import time
import pigpio
import logging
import signal
from enum import Enum
from typing import Union
from configparser import ConfigParser
from threading import Lock
from mdb import MDBHandler, MDBState, MDBVendRequest
from legireader import Legireader
from communicator import Communicator
from websocket import Websocket
from utils import get_config


class CoordinatorState(Enum):
    IDLE         = 1
    READING_CARD = 2
    LOADING_DATA = 3
    SUSPENDED    = 4
    SESSION      = 5


ERROR_DISPLAY_TIME  = 3500  # in milliseconds
CHEERS_DISPLAY_TIME = 3500  # in milliseconds

PID_FILE = '/run/beer.pid'


class Coordinator():
    """Coordinating the different threads and tasks."""

    def __init__(self, config: ConfigParser):
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        rx_gpio = int(config.get('mdb', 'rxGPIO'))
        tx_gpio = int(config.get('mdb', 'txGPIO'))

        self.pi = pigpio.pi()
        self.communicator = Communicator(config)
        self.legi = Legireader(config, self.handle_reader_start_reading, self.handle_reader_read_success, self.handle_reader_read_failure)
        self.mdb = MDBHandler(self.pi, rx_gpio, tx_gpio, self.handle_mdb_state_change, self.handle_mdb_vend_request, self.handle_mdb_vend_completed)
        self.websocket = Websocket()
        self.balance_lock = Lock()
        self.beer_slots = list(map(lambda x: int(x), config.get('default', 'beerSlots', fallback='').split(',')))
        self.session_timeout_s = int(config.get('default', 'sessionTimeout', fallback=8))
        self.session_timeout_ms = self.session_timeout_s*1000

        self.state_lock = Lock()
        self.state = CoordinatorState.IDLE
        self.state_changed = False
        self.rfid = None
        self.balance_available = None
        self.org = None
        self.pending_vend = False
        self.vend_completed = False
        self.time = time.time()
        self.cancelation_requested = False
        self.session_requested = False
        self.should_clear_display = False
        self.is_running = False


    def reset(self):
        self.logger.info('=== SIGHUP received! Resetting... ===')

        if not self.is_running:
            # Do nothing if Coordinator is not running.
            return

        try:
            if self.legi.isAlive():
                self.legi.reset()
            else:
                self.legi.start()
        except Exception as e:
            self.logger.exception('Failed to reset Legireader thread (Exception): {}'.format(e))
        try:
            if self.mdb.isAlive():
                self.mdb.reset()
            else:
                self.mdb.start()
        except Exception as e:
            self.logger.exception('Failed to reset MDBHandler thread (Exception): {}'.format(e))
        try:
            if self.communicator.isAlive():
                self.communicator.reset()
            else:
                self.communicator.start()
        except Exception as e:
            self.logger.exception('Failed to reset MDBHandler thread (Exception): {}'.format(e))


    def run(self):
        try:
            self.mdb.start()
            self.legi.start()
            self.communicator.start()
            self.websocket.start()

            self.is_running = True

            while self.is_running:
                if self.should_clear_display:
                    self.should_clear_display = False
                    self.clear_display_message()

                if self.state == CoordinatorState.SESSION:
                    if not self.cancelation_requested and not self.pending_vend:
                        if time.time() - self.time > self.session_timeout_s:
                            # Cancel Session after Timeout
                            self.mdb.cancel_session()
                            self.cancelation_requested = True
                            self.logger.debug('Session Timeout: Cancelling session!')
                        elif self.mdb.get_state() == MDBState.ENABLED and not self.session_requested and self.vend_completed and self.balance_available is not None and self.balance_available > 0:
                            self.session_requested = True
                            self.vend_completed = False
                            self.logger.debug('vend_completed set to FALSE')
                            self.mdb.open_session(self.get_free_beer_message(), self.session_timeout_ms  + 2000)
                            self.time = time.time()
                            self.logger.debug('Session reopened')
                elif self.state == CoordinatorState.READING_CARD and self.rfid is not None:
                    self.state = CoordinatorState.LOADING_DATA
                    self.logger.debug('Coordinator: new State is {}'.format(self.state))
                    self.logger.info('LEGI: Got Legi Data "' + str(self.rfid) + '"')
                    self.show_wait_message()
                    (state, balance_available, org) = self.communicator.get_available_beer_amount(self.rfid)

                    self.logger.info('DATA: (state, balance_available, org) = ({}, {}, {})'.format(state, str(balance_available), org))

                    if state == Communicator.RESPONSE_OK:
                        with self.balance_lock:
                            self.balance_available = balance_available
                            self.org = org
                        
                        if self.balance_available > 0:
                            self.mdb.open_session(self.get_free_beer_message(), self.session_timeout_ms + 2000)
                            self.time = time.time()
                        else:
                            self.mdb.update_display(self.get_free_beer_message(), ERROR_DISPLAY_TIME)

                            with self.balance_lock:
                                self.balance_available = None
                                self.rfid = None
                                self.org = None
                                self.state = CoordinatorState.IDLE
                                self.logger.debug('Coordinator: new State is {}'.format(self.state))
                    elif state == Communicator.AMPEL_RED:
                        self.rfid = None
                        self.state = CoordinatorState.IDLE
                        self.logger.debug('Coordinator: new State is {}'.format(self.state))
                        self.show_suspended_message()
                    elif state == Communicator.UNKNOWN_CARD:
                        self.rfid = None
                        self.state = CoordinatorState.IDLE
                        self.logger.debug('Coordinator: new State is {}'.format(self.state))
                        self.show_card_unknown_message()
                    else:
                        self.rfid = None
                        self.state = CoordinatorState.IDLE
                        self.logger.debug('Coordinator: new State is {}'.format(self.state))
                        self.show_unknown_error_message()

                time.sleep(0.2)
        except KeyboardInterrupt:
            pass

        self.logger.info('=== Stopping on user request ===')
        self.websocket.stop()
        self.mdb.stop()
        self.communicator.stop()
        self.legi.stop()
        if self.mdb.isAlive():
            self.mdb.join(5)
        if self.communicator.isAlive():
            self.communicator.join(5)
        if self.legi.isAlive():
            self.legi.join(5)
        self.logger.debug('All threads stopped!')
        self.is_running = False


    def get_free_beer_message(self) -> bytes:
        message = self.org.upper().encode('ascii').center(16)

        if self.balance_available == 1:
            message += (str(self.balance_available).encode('ascii') + b' free beer').center(16)
        else:
            message += (str(self.balance_available).encode('ascii') + b' free beers').center(16)
        return message


    def clear_display_message(self) -> None:
        self.mdb.update_display(b''.center(16) + b''.center(16), 100)


    def show_cheers_message(self) -> None:
        self.mdb.update_display(self.org.upper().encode('ascii').center(16) + (b'Enjoy your beer!').center(16), CHEERS_DISPLAY_TIME)


    def show_wait_message(self) -> None:
        self.mdb.update_display(b'Please wait'.center(16) + (b'').center(16), 10000)


    def show_reader_error_message(self) -> None:
        self.mdb.update_display(b'ERROR: Card'.center(16) + (b'not recognized').center(16), ERROR_DISPLAY_TIME)


    def show_suspended_message(self) -> None:
        self.mdb.update_display(b'No free beer!'.center(16) + (b'Ruum too dirty!').center(16), ERROR_DISPLAY_TIME)


    def show_card_unknown_message(self) -> None:
        self.mdb.update_display(b'Card is not'.center(16) + (b'registered!').center(16), ERROR_DISPLAY_TIME)


    def show_unknown_error_message(self) -> None:
        self.mdb.update_display(b'An unknown'.center(16) + (b'error occured!').center(16), ERROR_DISPLAY_TIME)


    def handle_reader_start_reading(self) -> None:
        with self.state_lock:
            if self.state == CoordinatorState.IDLE:
                self.state = CoordinatorState.READING_CARD
                self.logger.debug('Legireader: Card detected.')
                self.logger.debug('Coordinator: new State is {}'.format(self.state))
                self.show_wait_message()


    def handle_reader_read_success(self, uid: str, rfid: int) -> None:
        with self.state_lock:
            if self.state != CoordinatorState.SESSION and self.state != CoordinatorState.LOADING_DATA:
                self.logger.debug('Legireader: Received rfid {}'.format(rfid))
                self.logger.debug('tatatata: {}'.format(self.state))
                self.rfid = rfid


    def handle_reader_read_failure(self) -> None:
        with self.state_lock:
            if self.state == CoordinatorState.IDLE or self.state == CoordinatorState.READING_CARD:
                self.logger.debug('Legireader: Reader failed to read card!')
                self.state = CoordinatorState.IDLE
                self.logger.debug('Coordinator: new State is {}'.format(self.state))
                self.show_reader_error_message()


    def handle_mdb_state_change(self, newState: MDBState, previousState: MDBState) -> None:
        with self.state_lock:
            self.state_changed = True
            if (newState != MDBState.VEND and newState != MDBState.SESSION_IDLE):
                with self.balance_lock:
                    if (not self.vend_completed or self.cancelation_requested or (self.balance_available is not None and self.balance_available <= 0)):
                        self.balance_available = None
                        self.rfid = None
                        self.org = None
                        self.session_requested = False
                        self.cancelation_requested = False
                        self.state = CoordinatorState.IDLE
                        if not self.vend_completed:
                            self.should_clear_display = True
                        else:
                            self.should_clear_display = False
                        self.logger.debug('Coordinator: new State is {}'.format(self.state))
            else:
                self.state = CoordinatorState.SESSION
                self.logger.debug('Coordinator: new State is {}'.format(self.state))
                self.session_requested = False
                self.vend_completed = False
                self.logger.debug('vend_completed set to FALSE')


    def handle_mdb_vend_request(self, vend_request: MDBVendRequest) -> Union[bool, None]:
        if vend_request.slot not in self.beer_slots or self.pending_vend or self.cancelation_requested:
            # directly deny it if not a beer slot
            return False

        with self.balance_lock:
            if self.balance_available > 0:
                self.pending_vend = True
                self.show_cheers_message()
                self.websocket.trigger_cheers_image()
                return True
        return False


    def handle_mdb_vend_completed(self, successful: bool) -> None:
        with self.balance_lock:
            if successful:
                self.balance_available -= 1
                self.logger.debug('Dispensed a beer for RFID ' + str(self.rfid) + '(org: ' + self.org + ')')
                self.communicator.report_dispensed_beer(self.rfid, self.org)

                if self.pending_vend:
                    self.vend_completed = True
                    self.logger.debug('vend_completed set to TRUE')
                else:
                    self.vend_completed = False
                    self.logger.debug('vend_completed set to FALSE')
            else:
                self.vend_completed = False
                self.logger.debug('vend_completed set to FALSE')
            self.pending_vend = False
            self.time = time.time()



if __name__ == "__main__":
    pid = str(os.getpid())

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s\t%(levelname)s\t[%(name)s: %(funcName)s]\t%(message)s',
                        datefmt='%Y-%m-%d %I:%M:%S')

    if os.path.isfile(PID_FILE):
        logging.warning('{} already exists, exiting'.format(PID_FILE))
        sys.exit()

    f = open(PID_FILE, 'w')
    f.write(pid)
    f.close()

    logging.info('===================')
    logging.info('PID: {}'.format(pid))
    logging.info('===================')

    try:
        config = get_config('base')

        coordinator = Coordinator(config)

        def sighup_handler(signalNumber, frame):
            coordinator.reset()

        signal.signal(signal.SIGHUP, sighup_handler)
        coordinator.run()
    finally:
        os.unlink(PID_FILE)
