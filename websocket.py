
import asyncio
import websockets
from threading import Thread, Lock
import logging

class Websocket(Thread):
    """Class to communicate with the beerlog display via a websocket"""

    def __init__(self) -> None:

        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        self.connections = set()

        Thread.__init__(self, daemon=True)
    
    async def websocketHandler(self, websocket):
        self.connections.add(websocket)
        await websocket.wait_closed()
        self.connections.remove(websocket)

    async def startWebsocket(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        self.stopFuture = asyncio.Future()
        async with websockets.serve(self.websocketHandler, "127.0.0.1", 1234):
            await self.stopFuture

    def run(self):
        asyncio.run(self.startWebsocket())

    def report_dispensed_beer(self, rfid, org):
        websockets.broadcast(self.connections, f"{{'rfid': '{rfid}', 'org': '{org}'}}")
        
    def trigger_cheers_image(self):
        websockets.broadcast(self.connections, "beer_triggered")

    def stop(self):
        """Terminate the thread."""
        self.logger.info("Stopping websocket server...")
        self.stopFuture.cancel()